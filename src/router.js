import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import About from './views/About.vue'
import Semaine1Jour1 from './views/semaine1jour1.vue'
import Semaine1Jour2 from './views/semaine1jour2.vue'
import Semaine1Jour3 from './views/semaine1jour3.vue'
import Semaine1Jour4 from './views/semaine1jour4.vue'
import Semaine1Jour5 from './views/semaine1jour5.vue'
import Semaine2Jour1 from './views/semaine2jour1.vue'
import Semaine2Jour2 from './views/semaine2jour2.vue'
import Semaine2Jour3 from './views/semaine2jour3.vue'
import Semaine2Jour4 from './views/semaine2jour4.vue'
import Semaine2Jour5 from './views/semaine2jour5.vue'



Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '/s1j1',
      name: 's1j1',
      component: Semaine1Jour1
    },
    {
      path: '/s1j2',
      name: 's1j12',
      component: Semaine1Jour2
    },
    {
      path: '/s1j3',
      name: 's1j3',
      component: Semaine1Jour3
    },
    {
      path: '/s1j4',
      name: 's1j4',
      component: Semaine1Jour4
    },
    {
      path: '/s1j5',
      name: 's1j5',
      component: Semaine1Jour5
    },
    {
      path: '/s2j1',
      name: 's2j1',
      component: Semaine2Jour1
    },
    {
      path: '/s2j2',
      name: 's2j2',
      component: Semaine2Jour2
    },
    {
      path: '/s2j3',
      name: 's2j3',
      component: Semaine2Jour3
    },
    {
      path: '/s2j4',
      name: 's2j4',
      component: Semaine2Jour4
    },
    {
      path: '/s2j5',
      name: 's2j5',
      component: Semaine2Jour5
    }
  ]
})
